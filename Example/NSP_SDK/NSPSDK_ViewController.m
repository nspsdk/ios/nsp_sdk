//
//  NSPSDK_ViewController.m
//  NSP_SDK
//
//  Created by guillaume@bigsool.com on 12/12/2020.
//  Copyright (c) 2020 guillaume@bigsool.com. All rights reserved.
//

#import "NSPSDK_ViewController.h"
#import "NSPSDK_AppDelegate.h"

@interface NSPSDK_ViewController ()
@end

@implementation NSPSDK_ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    NSMutableDictionary * mutableDic = [[NSMutableDictionary alloc]init];
        
    [mutableDic setObject:@"VALUE_1" forKey:@"KEY_1"];
    [mutableDic setObject:@"VALUE_2" forKey:@"KEY_2"];
    NSPSDK_AppDelegate* appDelegate = (NSPSDK_AppDelegate*) [[UIApplication sharedApplication] delegate];
    
    [appDelegate.core send_trackingWithtitle:@"TEST_SPEC" AndStrClass:@"VIEW" AndEventType:@"VIEW" From:@"NSP_FROM" To:@"NSP_TO" andDic:mutableDic];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
