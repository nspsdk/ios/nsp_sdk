//
//  NSPSDK_AppDelegate.h
//  NSP_SDK
//
//  Created by guillaume@bigsool.com on 12/12/2020.
//  Copyright (c) 2020 guillaume@bigsool.com. All rights reserved.
//

@import UIKit;
@import FirebaseMessaging;

#import <NspCore.h>

@interface NSPSDK_AppDelegate : UIResponder <UIApplicationDelegate, UNUserNotificationCenterDelegate, FIRMessagingDelegate>

@property (strong, nonatomic) UIWindow *window;
@property (nonatomic, retain) NspCore* core;

@end
