//
//  NSPSDK_AppDelegate.m
//  NSP_SDK
//
//  Created by guillaume@bigsool.com on 12/12/2020.
//  Copyright (c) 2020 guillaume@bigsool.com. All rights reserved.
//

#import "NSPSDK_AppDelegate.h"

@import FirebaseCore;
@import FirebaseMessaging;

@implementation NSPSDK_AppDelegate
@synthesize core;

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    core = [[NspCore alloc] initWithAppId:@"APP_ID" andGDPROption:GDPR_ANON];
    [core setLogin:@"LOGIN"];
    [core startup];
        
    FIROptions* options = [[FIROptions alloc] initWithGoogleAppID: @"GOOGLE_APP_ID" GCMSenderID: @"GCM_SENDER_ID"];
    [options setAPIKey: @"API_KEY"];
    [options setProjectID: @"PROJECT_ID"];
    
    [FIRApp configureWithOptions:options];
    
    [UNUserNotificationCenter currentNotificationCenter].delegate = self;
    UNAuthorizationOptions authOptions = UNAuthorizationOptionAlert |
        UNAuthorizationOptionSound | UNAuthorizationOptionBadge;
    [[UNUserNotificationCenter currentNotificationCenter]
        requestAuthorizationWithOptions:authOptions
        completionHandler:^(BOOL granted, NSError * _Nullable error) {
          // ...
        }];

    [application registerForRemoteNotifications];
    
    [FIRMessaging messaging].delegate = self;
    
    return YES;
}

- (void)messaging:(FIRMessaging *)messaging didReceiveRegistrationToken:(NSString *)fcmToken {
    NSLog(@"FCM registration token: %@", fcmToken);
    
    [core setNotificationToken: fcmToken];
}

- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

@end
