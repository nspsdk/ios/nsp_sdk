//
//  main.m
//  NSP_SDK
//
//  Created by guillaume@bigsool.com on 12/12/2020.
//  Copyright (c) 2020 guillaume@bigsool.com. All rights reserved.
//

@import UIKit;
#import "NSPSDK_AppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([NSPSDK_AppDelegate class]));
    }
}
