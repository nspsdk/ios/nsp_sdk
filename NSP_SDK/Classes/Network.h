//
//  Network.h
//  NspSdk_Objc
//
//  Created by maaks on 14/04/2019.
//  Copyright © 2019 maaks. All rights reserved.
//

#import <Foundation/Foundation.h>
@class NspCore;
#import <CoreData/CoreData.h>
#import "CoreDataStack.h"
@class DataToPush;

NS_ASSUME_NONNULL_BEGIN

@interface Network : NSObject

@property NspCore* core;
@property NSMutableArray *dtpList;

-(id)initWithCore:(NspCore*)core;
-(void)feedDtpList;
-(void)synchronize:(id)lockObj WithBlock:(void(^)(void)) block;
-(void)send_dataWithParameterString: (NSString*)parameterString AndCallback:(NSString*)callback AndUrl:(NSString*)url;
-(void)networkLoop;

@end

NS_ASSUME_NONNULL_END
