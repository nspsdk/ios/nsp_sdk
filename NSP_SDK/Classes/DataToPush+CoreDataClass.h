//
//  DataToPush+CoreDataClass.h
//  NSP_SDK
//
//  Created by Guillaume Gonzales on 30/12/2020.
//
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

NS_ASSUME_NONNULL_BEGIN

@interface DataToPush : NSManagedObject

@end

NS_ASSUME_NONNULL_END

#import "DataToPush+CoreDataProperties.h"
