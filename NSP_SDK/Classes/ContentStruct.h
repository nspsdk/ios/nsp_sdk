//
//  ContentStruct.h
//  NspSdk_Objc
//
//  Created by maaks on 12/04/2019.
//  Copyright © 2019 maaks. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface ContentStruct : NSObject

-(id)init;
@property NSString *id;
@property NSString *type;
@property NSString *title;
@property NSString *subtitle;
@property NSString *image;
@property NSString *thumbnail;
@property NSString *expirationDate;
@property NSString *content;
@property NSString *sharingtext;
@property NSString *sharinglink;

@end

NS_ASSUME_NONNULL_END
