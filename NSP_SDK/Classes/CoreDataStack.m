//
//  CoreDataStack.m
//  NspSdk_Objc
//
//  Created by maaks on 12/04/2019.
//  Copyright © 2019 maaks. All rights reserved.
//

#import "CoreDataStack.h"

@implementation CoreDataStack

static CoreDataStack* _sharedInstance;


+(CoreDataStack *)sharedInstance{
    if(_sharedInstance == nil)
    {
        _sharedInstance = [[CoreDataStack alloc]init];
    }
        
    return _sharedInstance;}
+ (void)setSharedInstance:(CoreDataStack *)sharedInstance { _sharedInstance = sharedInstance; }


-(id)init
{
    NSString* momdName = @"DataModel";
    NSBundle* bundle = [NSBundle bundleForClass:[self class]];
    
    NSURL *modelURL = [bundle URLForResource:momdName withExtension:@"momd"];
    NSAssert(modelURL, @"Failed to locate momd bundle in application");
    // The managed object model for the application. It is a fatal error for the application not to be able to find and load its model.
    self.managedObjectModel = [[NSManagedObjectModel alloc] initWithContentsOfURL:modelURL];
    NSAssert(self.managedObjectModel, @"Failed to initialize mom from URL: %@", modelURL);
    
    self.persistentContainer = [[NSPersistentContainer alloc] initWithName:momdName managedObjectModel:self.managedObjectModel];
    [self.persistentContainer loadPersistentStoresWithCompletionHandler:^(NSPersistentStoreDescription *description, NSError *error) {
        if (error != nil) {
            NSLog(@"Failed to load Core Data stack: %@", error);
            abort();
        }
    }];
    
    NSArray<NSURL*>* arr = [[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask];
    
    self.applicationDocumentsDirectory = [arr objectAtIndex:[arr count]-1];
    
    
    self.persistentStoreCoordinator = [[NSPersistentStoreCoordinator alloc]initWithManagedObjectModel:self.managedObjectModel];
    NSURL* url = [self.applicationDocumentsDirectory URLByAppendingPathComponent:@"SingleViewCoreData.sqlite"];
    NSError *error = nil;
    [self.persistentStoreCoordinator addPersistentStoreWithType:NSSQLiteStoreType configuration:nil URL:url options:nil error:&error];
    
    self.managedObjectContext = [[NSManagedObjectContext alloc] initWithConcurrencyType:NSMainQueueConcurrencyType];
    self.managedObjectContext.persistentStoreCoordinator = self.persistentStoreCoordinator;
    
    CoreDataStack.sharedInstance=self;
    return self;
}

-(void) saveContext
{
    
    if (@available(iOS 10, *))
    {
        if([self.persistentContainer.viewContext hasChanges])
        {
            NSError* err = nil;
            [self.persistentContainer.viewContext save:&err];
            if(err!=nil)
            {
                
            }
        }
    }
    else
    {
        if(self.managedObjectContext.hasChanges)
        {
            NSError* err = nil;
            [self.managedObjectContext save:&err];
            if(err!=nil)
            {
                abort();
            }
        }
    }
    
    
    
}

@end
