//
//  NspCore.h
//  NspSdk_Objc
//
//  Created by maaks on 12/04/2019.
//  Copyright © 2019 maaks. All rights reserved.
//

#ifndef NspCore_h
#define NspCore_h
#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>
#import <CoreTelephony/CTCarrier.h>
#import <CoreTelephony/CTTelephonyNetworkInfo.h>
#import <CoreData/CoreData.h>
#import "Network.h"
#import "ContentStruct.h"
#import "Settings.h"
#import <sys/utsname.h>
#import <UIKit/UIKit.h>
#import "Geopoint.h"

@protocol NspCoreProtocol
    -(void) geoPointsDidLoad;
@end 

@interface NspCore : NSObject<CLLocationManagerDelegate>

typedef NS_ENUM(NSUInteger, GDPROptions) {
    GDPR_FULL = 1,
    GDPR_ANON = 2,
    GDPR_NONE = 3
};

@property GDPROptions selectedGDPR;
@property NSMutableArray* listeners;
@property CLLocationManager* locationManager;
@property Network* network;
@property NSMutableArray* geopoints;
@property (nonatomic) NSString* login;
@property Settings* settings;

-(void) setLogin: (NSString*) login;
-(void) clearLogin;
-(NSMutableArray*)getGeoPointsArray;
-(void) registerListener: (id<NspCoreProtocol>) listener;
-(int) getOSInfo;
-(id) initWithAppId:(NSString*)appId andGDPROption:(GDPROptions)gdpr;
-(id) initWithAppId:(NSString*)appId andGDPROption:(GDPROptions)gdpr andPushUrl:(NSString*)pushUrl;
-(bool) startup;
-(void) enableGeoZone;
-(void) getGeozones;
-(void) setGeoZoneManuallyWithId:(NSString*) rid andWay:(NSString*) way;
-(void) handleEventForRegion:(CLRegion*)region andWay:(NSString*) way;
-(void) send_notifyWithStr_action:(NSString*) Str_action AndStr_label: (NSString*) Str_label;
-(void) updateContent;
-(NSMutableArray*) getContentDic;
-(ContentStruct*) getContent:(NSString*)cid;
-(void) getContents;
-(void) send_trackingWithtitle:(NSString*)title AndStrClass:(NSString*)strClass AndEventType:(NSString*) eventType From:(NSString*)from To:(NSString*) to andDic :(NSDictionary<NSString*, NSString*>*) dic;

-(void) createUserWithDic :(NSDictionary<NSString *, NSString*>*)dic;
-(void) updateUserWithDic :(NSDictionary<NSString *, NSString*>*)dic;
-(CLCircularRegion*) createRegionWithSite:(NSDictionary*)site;

-(void) callbackWithStringData:(NSString*) callbackData andData:(NSData*)data;
-(void) setDeviceToken:(NSData*) token;
-(void) setNotificationToken:(NSString*) token;
-(void)saveSettings;
@end




#endif /* NspCore_h */
