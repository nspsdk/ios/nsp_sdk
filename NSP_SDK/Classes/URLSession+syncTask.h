//
//  URLSession+syncTask.h
//  NspSdk_Objc
//
//  Created by maaks on 15/04/2019.
//  Copyright © 2019 maaks. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSURLSession(syncTask)

+ (NSData *)synchronousDataTaskWithUrl:(NSURLRequest *)request
                     returningResponse:(__autoreleasing NSURLResponse **)responsePtr
                                 error:(__autoreleasing NSError **)errorPtr;

@end
