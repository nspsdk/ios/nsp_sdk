//
//  Settings.m
//  NspSdk_Objc
//
//  Created by maaks on 14/04/2019.
//  Copyright © 2019 maaks. All rights reserved.
//

#import "Settings.h"

@implementation Settings


-(id)init
{
    self.clientId =@"";
    self.appid =@"4D088AA10921B22147B0DB67DB9D5BB8";
    self.version=@"0.6";
    self.token=@"";
    self.notification_token=@"";
    self.pushUrl = @"https://ws3.smartp.com/sp_pushMobile.cfm";
    self.geozoneRefreshInterval = 3000;
    self.lastContent=@"NULL";
    self.contents = [[NSMutableDictionary<NSString*,ContentStruct*> alloc]init];
    
    self.osLanguage=@"";
    self.device=@"";
    self.career=@"";
    self.osVersion=@"";
    self.appVersion=@"";
    self.screeSize=@"";
    self.ipAddress=@"127.0.0.1";
    self.sdkVersion=@"1.0";
    
    NSTimeZone *timeZone = [NSTimeZone localTimeZone];
    self.timeZone = [timeZone abbreviation];
    self.wasSet=false;
    
    
    return self;
}

+(id) recursiveMutable:(id)object
{
    if([object isKindOfClass:[NSDictionary class]])
    {
        NSMutableDictionary* dict = [NSMutableDictionary dictionaryWithDictionary:object];
        for(NSString* key in [dict allKeys])
        {
            [dict setObject:[Settings recursiveMutable:[dict objectForKey:key]] forKey:key];
        }
        return dict;
    }
    else if([object isKindOfClass:[NSArray class]])
    {
        NSMutableArray* array = [NSMutableArray arrayWithArray:object];
        for(int i=0;i<[array count];i++)
        {
            [array replaceObjectAtIndex:i withObject:[Settings recursiveMutable:[array objectAtIndex:i]]];
        }
        return array;
    }
    else if([object isKindOfClass:[NSString class]])
        return [NSMutableString stringWithString:object];
    return object;
}

@end
