//
//  Geopoint.h
//  NspSdk_Objc
//
//  Created by maaks on 12/04/2019.
//  Copyright © 2019 maaks. All rights reserved.
//


#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN
@interface Geopoint : NSObject

@property NSString *name;
@property NSString *loc;
@property NSString *gid;

-(id)init;
-(id)initWithName: (NSString*) name andL:(NSString*)l andG: (NSString*)g;
-(NSString*)getName;
-(NSString*)getLoc;
-(NSString*)getGid;

@end

NS_ASSUME_NONNULL_END


