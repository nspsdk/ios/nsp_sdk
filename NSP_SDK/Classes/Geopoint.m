//
//  Geopoint.m
//  NspSdk_Objc
//
//  Created by maaks on 12/04/2019.
//  Copyright © 2019 maaks. All rights reserved.
//

#import "Geopoint.h"

@implementation Geopoint


-(id)init
{
    return [super init];
}
-(id)initWithName: (NSString*) name andL:(NSString*)l andG: (NSString*)g
{
    id var = [super init];
    self.name = name;
    self.loc = l;
    self.gid = g;
    return var;
}
-(NSString*)getName
{
    return [self name];
}

-(NSString*)getLoc
{
    return [self loc];
}

-(NSString*)getGid
{
    return [self gid];
}


@end
