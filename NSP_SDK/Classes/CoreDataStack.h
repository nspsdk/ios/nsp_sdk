//
//  CoreDataStack.h
//  NspSdk_Objc
//
//  Created by maaks on 12/04/2019.
//  Copyright © 2019 maaks. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>
#import <UIKit/UiKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface CoreDataStack : NSObject

@property (class)CoreDataStack* sharedInstance;
@property NSPersistentContainer* persistentContainer;
@property NSURL* applicationDocumentsDirectory;
@property NSManagedObjectModel* managedObjectModel;
@property NSPersistentStoreCoordinator* persistentStoreCoordinator;
@property NSManagedObjectContext* managedObjectContext;


-(id)init;


@end

NS_ASSUME_NONNULL_END
