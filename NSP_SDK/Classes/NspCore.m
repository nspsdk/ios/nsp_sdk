//
//  NspCore.m
//  NspSdk_Objc
//
//  Created by maaks on 12/04/2019.
//  Copyright © 2019 maaks. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "NspCore.h"
#include "TargetConditionals.h"
#define IDIOM    UI_USER_INTERFACE_IDIOM()
#define IPAD     UIUserInterfaceIdiomPad
#define IPHONE   UIUserInterfaceIdiomPhone
@implementation NspCore
@synthesize login = _login;

static NspCore *sharedInstance = nil;

-(void) clearLogin
{
    self.login=@"";
}

- (NSString*) login {
    return _login;
}

-(void) setLogin: (NSString*) login {
    if (_login == login) return;
        _login = login;
    [self send_trackingWithtitle:@"Update Notification Token" AndStrClass:@"NOTIF" AndEventType:@"NOTIF" From:nil To:nil andDic:nil];
}

-(NSMutableArray*)getGeoPointsArray
{
    return self.geopoints;
}

-(void) registerListener: (id<NspCoreProtocol>) listener
{
    [self.listeners addObject:listener];
}

-(int) getOSInfo
{
    return (int)[NSProcessInfo processInfo].operatingSystemVersion.majorVersion;
}

-(id) initWithAppId:(NSString*)appId andGDPROption:(GDPROptions)gdpr {
    return [self initWithAppId:appId andGDPROption:gdpr andPushUrl:@""];
}

-(id) initWithAppId:(NSString*)appId andGDPROption:(GDPROptions)gdpr andPushUrl:(NSString*)pushUrl
{
    /*if (sharedInstance == nil) {
        sharedInstance = [super init];
    }
    self = sharedInstance;*/
    self = [super init];
    
    _selectedGDPR = gdpr;
    if([self getOSInfo]<10)
    {
        return nil;
    }
    
    self.settings = [[Settings alloc]init];
    self.settings.appid = appId;
    if (![pushUrl isEqualToString:@""]) {
        self.settings.pushUrl = pushUrl;
    }
    [self loadSettings];
    NSUserDefaults* defaults = NSUserDefaults.standardUserDefaults;
    NSString* cid= [defaults stringForKey:@"cid"];
    if(cid==nil)
    {
        cid =  [[NSUUID alloc]init].UUIDString;
        [defaults setObject:cid forKey:@"cid"];
    }
    self.settings.clientId = cid;
    
    
    struct utsname systemInfo;
    uname(&systemInfo);
    
    self.settings.device = [NSString stringWithCString:systemInfo.machine
                              encoding:NSUTF8StringEncoding];
    
    
    
#if TARGET_OS_SIMULATOR
    self.settings.career = @"Simulator";
#else
    if(IDIOM==IPHONE)
    {
        CTTelephonyNetworkInfo *info = [[CTTelephonyNetworkInfo alloc] init];
        if (@available(iOS 12.0, *))
        {
            CTCarrier *carrier = nil;
            if (info.serviceSubscriberCellularProviders.allKeys.count > 0) {
                carrier = info.serviceSubscriberCellularProviders[info.serviceSubscriberCellularProviders.allKeys[0]];
            }
            
            if(carrier == nil || carrier.carrierName == nil)
            {
                self.settings.career = @"none";
            }
            else
            {
                self.settings.career = (carrier.carrierName);
            }
        }
        else
        {
            self.settings.career = @"unavailable";
        }
    }
    else
    {
        self.settings.career = @"ipad";
    }
    
#endif
    
    
    self.settings.osVersion = [UIDevice currentDevice].systemVersion;
    //DISCOVER OS VERSION
    
    self.settings.appVersion = @"1.0.1";
    
    // DISCOVER SCREEN SIZE :
    CGRect screenSize = UIScreen.mainScreen.bounds;
    
    NSLocale *locale = [NSLocale currentLocale];
    self.settings.osLanguage = [locale objectForKey:NSLocaleLanguageCode];
    
    self.settings.screeSize = [NSString stringWithFormat:@"%dx%d", (int)round(screenSize.size.width),(int)round(screenSize.size.height)];
    
    NSString* deviceTokenKey = @"DeviceToken";
    
    if([defaults objectForKey:deviceTokenKey]!=nil)
    {
        self.settings.token = [defaults objectForKey:deviceTokenKey];
    }
    
    
    return self;
}



-(void)loadSettings
{
    NSUserDefaults *ud =[NSUserDefaults standardUserDefaults];
    self.settings.pushUrl = [ud objectForKey:@"pushUrl"];
    self.settings.geozoneRefreshInterval = [[ud objectForKey:@"geozoneRefreshInterval"] intValue];
    if(self.settings.pushUrl!=nil)
    {
        self.settings.wasSet =true;
    }
}
-(void)saveSettings
{
    NSUserDefaults *ud =[NSUserDefaults standardUserDefaults];
    [ud setObject:self.settings.pushUrl forKey:@"pushUrl"];
    [ud setObject:[NSString stringWithFormat:@"%ld",(long)self.settings.geozoneRefreshInterval] forKey:@"geozoneRefreshInterval"];
    [ud setObject:@"TRUE" forKey:@"wasSet"];
    [ud synchronize];
}
-(bool) startup
{
    self.network = [[Network alloc]initWithCore:self];
 //   [self updateContent];
    return true;
}

-(void) enableGeoZone
{
    self.locationManager.delegate=self;
    [self getGeozones];// fonction maj tout les geozoneRefreshInterval secondes
}

-(void)getGeozones
{
    if (self.settings.pushUrl == nil || self.settings.appid == nil || [self.settings.appid isEqualToString:@""]) {
        return;
    }
    
    NSString* serverUrl = self.settings.pushUrl;
    NSString* parameterString = @"{\"subaction\":\"getGeozones\"}";
    parameterString = [NSString stringWithFormat:@"{\"app\":\"%@\",\"data\":%@}",self.settings.appid,parameterString];
    NSURL* url = [[NSURL alloc]initWithString:serverUrl];
    NSURLSession* session = [NSURLSession sharedSession];
    NSMutableURLRequest* request = [[NSMutableURLRequest alloc]initWithURL:url];
 
    
    request.HTTPMethod = @"POST";
    [request setValue:@"application/json" forHTTPHeaderField: @"Accept"];
    [request setValue:@"application/json; charset=utf-8" forHTTPHeaderField: @"Content-Type"];
    request.cachePolicy = NSURLRequestReloadIgnoringCacheData;
    request.HTTPBody = [parameterString dataUsingEncoding:NSUTF8StringEncoding];
    
    
    void (^completionHandler)(NSData * __nullable data, NSURLResponse * __nullable response, NSError * __nullable error);
    completionHandler = ^(NSData * __nullable data, NSURLResponse * __nullable response, NSError * __nullable error)
    {
        if(error!=nil) NSLog(@"error %@",error);
        else
        {
            NSError* err =nil;
            NSDictionary *json = [NSJSONSerialization JSONObjectWithData:data options:0 error:&err];
            if(err!=nil)
            {
                NSArray* geozones =[json objectForKey:@"sites"];
                if(geozones!=nil)
                {
                    for(int i=0 ; i<[geozones count];i++)
                    {
                        NSDictionary* site = [geozones objectAtIndex:i];
                        CLCircularRegion* region = [self createRegionWithSite:site];
                        [self.locationManager startMonitoringForRegion:region];
                        NSLog(@"%@",site);
                        NSString* sid = [site objectForKey:@"siteID"];
                        Geopoint* g =[[Geopoint alloc]initWithName:[site objectForKey:@"name"] andL:[site objectForKey:@"address"] andG:sid];
                        [self.geopoints addObject: g];
                        
                    }
                }
                    
                
                for(int i = 0 ; i<[self.listeners count];i++)
                {
                    id<NspCoreProtocol> cp = [self.listeners objectAtIndex:i];
                    [cp geoPointsDidLoad];
                }
            }
            else NSLog(@"error %@",err);
        }
    };
    
    NSURLSessionDataTask* task =  [session dataTaskWithRequest:request completionHandler:completionHandler];
    [task resume];
    
}

-(void) setGeoZoneManuallyWithId:(NSString*) rid andWay:(NSString*)way
{
    NSString* parameterString = [NSString stringWithFormat: @"{\"subaction\":\"proximityReport\",\"userToken\":\"%@\",\"major\":\"%@\",\"minor\":\"0\",\"way\":\"%@\"}",self.settings.clientId,rid,way];
    
    parameterString = [NSString stringWithFormat:@"{\"app\":\"%@\",\"data\":%@}",self.settings.appid, parameterString];
    [self.network send_dataWithParameterString: parameterString AndCallback: @"handleEvent" AndUrl: self.settings.pushUrl];
    
}

-(void) handleEventForRegion:(CLRegion*)region andWay:(NSString*) way
{
    NSString* parameterString = [NSString stringWithFormat:@"{\"subaction\":\"proximityReport\",\"userToken\":\"%@\",\"major\":\"%@\",\"minor\":\"0\",\"way\":\"%@\"}",self.settings.clientId,region.identifier,way];
    
    parameterString = [NSString stringWithFormat:@"{\"app\":\"%@\",\"data\":%@}",self.settings.appid, parameterString];
    [self.network send_dataWithParameterString: parameterString AndCallback: @"handleEvent" AndUrl: self.settings.pushUrl];
}
-(void) send_notifyWithStr_action:(NSString*) Str_action AndStr_label: (NSString*) Str_label
{
    NSString* parameterString = [NSString stringWithFormat: @"{\"subaction\":\"notifyAction\",\"userToken\":\"%@\",\"action\":\"%@\",\"label\":\"%@\"}",self.settings.clientId,Str_action,Str_label];
    parameterString = [NSString stringWithFormat:@"{\"app\":\"%@\",\"data\":%@}",self.settings.appid, parameterString];
    [self.network send_dataWithParameterString: parameterString AndCallback: @"notify" AndUrl: self.settings.pushUrl];
    
}

-(void) updateContent
{
    [self getContents];
}

-(NSMutableArray*) getContentDic
{
    NSMutableArray* contents_arr = [[NSMutableArray alloc]init];
    for(int i = 0 ; i<[self.settings.contents count];i++)
    {
        [contents_arr addObject:[[self.settings.contents allValues] objectAtIndex:i]];
    }
    return contents_arr;
}

-(ContentStruct*) getContent:(NSString*)cid
{
    NSString* parameterString = [NSString stringWithFormat: @"{\"subaction\":\"getContent\",\"userToken\":\"%@\",\"content\":\"%@\"}",self.settings.clientId,cid];
    parameterString = [NSString stringWithFormat:@"{\"app\":\"%@\",\"data\":%@}",self.settings.appid, parameterString];
    [self.network send_dataWithParameterString: parameterString AndCallback: @"getContent" AndUrl: self.settings.pushUrl];
    return [self.settings.contents objectForKey:cid];
}

-(void) getContents
{
    NSString* parameterString = [NSString stringWithFormat: @"{\"subaction\":\"getContents\",\"userToken\":\"%@\",\"lastContent\":\"%@\"}",self.settings.clientId,self.settings.lastContent];
    parameterString = [NSString stringWithFormat:@"{\"app\":\"%@\",\"data\":%@}",self.settings.appid, parameterString];
    [self.network send_dataWithParameterString: parameterString AndCallback: @"getContent" AndUrl: self.settings.pushUrl];
    
}


-(void) send_trackingWithtitle:(NSString*)title AndStrClass:(NSString*)strClass AndEventType:(NSString*) eventType From:(NSString*)from To:(NSString*) to andDic :(NSDictionary<NSString*, NSString*>*) dic
{
    NSMutableString* params = [[NSMutableString alloc] initWithString:@"\"params\":{"] ;
    bool firstone = true;
    
    if(from!=nil && to!=nil)
    {
        [params appendFormat:@"\"to\":\"%@\",\"from\":\"%@\"",to,from];
        firstone=false;
    }
    
    if(dic!=nil)
    {
        for(int i=0 ; i<[dic count];i++)
        {
            if(!firstone)
                [params appendString:@","];
            else
                firstone = false;
            [params appendFormat:@"\"%@\":\"%@\"",[[dic allKeys]objectAtIndex:i],[dic objectForKey:[[dic allKeys]objectAtIndex:i]]];
        }
    }
    
    [params appendString:@"},"];
    
    
    
    NSMutableString* data = [[NSMutableString alloc]initWithString:@"\"content\":{\"data\":[{"];
    [data appendString:@"\"sdkSource\":\"IOS\","];
    [data appendFormat:@"\"class\":\"%@\",",strClass];
    [data appendFormat:@"\"userID\":\"%@\",",self.settings.clientId];
    [data appendString:@"\"userAgent\":\"IOS\","];
    [data appendString:@"\"_site\":\"app ios\","];
    [data appendFormat:@"\"eventType\":\"%@\",",eventType];
    [data appendFormat:@"\"osLanguage\":\"%@\",",self.settings.osLanguage];
    if (![self.settings.notification_token isEqualToString:@""]) {
        [data appendFormat:@"\"notificationToken\":\"%@\",",self.settings.notification_token];
    }
    if(!firstone)
    {
        [data appendString:params];
    }
    if(self.login!=nil)
    {
        [data appendFormat:@"\"login\":\"%@\",",self.login];
    }
    [data appendFormat:@"\"osVersion\":\"%@\",",self.settings.osVersion];
    [data appendFormat:@"\"title\":\"%@\",",title];
    [data appendFormat:@"\"carreer\":\"%@\",",self.settings.career];
    [data appendFormat:@"\"date\":\"%@\",",[self getUtcDate]];
    [data appendFormat:@"\"hid\":\"%@\",",self.settings.appid];
    [data appendFormat:@"\"timezone\":\"%@\",",self.settings.timeZone];
    [data appendFormat:@"\"device\":\"%@\",",self.settings.device];
    [data appendFormat:@"\"screenSize\":\"%@\",",self.settings.screeSize];
    [data appendFormat:@"\"sdkVersion\":\"%@\"}]}",self.settings.sdkVersion];
    
    NSString* parameterString = [NSString stringWithFormat: @"{\"subaction\":\"tracking\",\"userToken\":\"%@\",%@}" ,self.settings.clientId,data];
    parameterString = [NSString stringWithFormat: @"{\"app\":\"%@\",\"data\":%@}",self.settings.appid,parameterString];
    [self.network send_dataWithParameterString: parameterString AndCallback: @"tracking" AndUrl: self.settings.pushUrl];
    
    
    
}

-(NSString*) getUtcDate
{
    NSDate *currentDate = [[NSDate alloc] init];
    NSTimeZone *timeZone = [NSTimeZone timeZoneWithName:@"UTC"];
    // or Timezone with specific name like
    // [NSTimeZone timeZoneWithName:@"Europe/Riga"] (see link below)
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setTimeZone:timeZone];
    [dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"];
    return [dateFormatter stringFromDate:currentDate];
    
}

-(void) createUserWithDic :(NSDictionary<NSString *, NSString*>*)dic
{
    NSMutableString* fistString = [NSMutableString stringWithFormat:@"{\"subaction\":\"createUser\",\"notificationToken\":\"%@\"",self.settings.token];
    
    if(dic!=nil)
    {
        for(int i = 0 ; i<[dic count];i++)
        {
            [fistString appendFormat:@",\"%@\":\"%@\"",[[dic allKeys] objectAtIndex:i],[[dic allValues]objectAtIndex:i]];
        }
    }
    
    NSString* parameterString = [NSString stringWithFormat:@"{\"app\":\"%@\",\"data\":%@}",self.settings.appid, fistString];
    [self.network send_dataWithParameterString: parameterString AndCallback: @"createUser" AndUrl: self.settings.pushUrl];
}

-(void) updateUserWithDic :(NSDictionary<NSString *, NSString*>*)dic
{
    NSMutableString* fistString = [NSMutableString stringWithFormat:@"{\"subaction\":\"updateUser\",\"notificationToken\":\"%@\"",self.settings.token];
    
    if(dic!=nil)
    {
        for(int i = 0 ; i<[dic count];i++)
        {
            [fistString appendFormat:@",\"%@\":\"%@\"",[[dic allKeys] objectAtIndex:i],[[dic allValues]objectAtIndex:i]];
        }
    }
    
    NSString* parameterString = [NSString stringWithFormat:@"{\"app\":\"%@\",\"data\":%@}",self.settings.appid, fistString];
    [self.network send_dataWithParameterString: parameterString AndCallback: @"createUser" AndUrl: self.settings.pushUrl];
}

-(CLCircularRegion*) createRegionWithSite:(NSDictionary*)site
{
    CLLocationDegrees lat = [[site objectForKey:@"lat"]doubleValue];
    CLLocationDegrees lon = [[site objectForKey:@"lon"]doubleValue];
    CLLocationDistance radius = [[site objectForKey:@"size"]doubleValue];
    CLLocationCoordinate2D coordinate = CLLocationCoordinate2DMake(lat, lon);
    
    CLCircularRegion* region = [[CLCircularRegion alloc] initWithCenter:coordinate radius:radius identifier:[site objectForKey:@"siteID"]];
    return region;
}

-(void) callbackWithStringData:(NSString*) callbackData andData:(NSData*)data
{
    NSError* error=nil;
    NSDictionary* object = [NSJSONSerialization JSONObjectWithData:data options:0 error:&error];
    if(error!=nil)
    {
        return;
    }
    NSUserDefaults* defaults = NSUserDefaults.standardUserDefaults;
    
    if(object!=nil)
    {
        if([callbackData isEqualToString:@"createUser"])
        {
            NSString*token = ([object objectForKey:@"userToken"]);
            if(token!=nil)
            {
                [defaults setObject:token forKey:@"cid"];
                self.settings.clientId = token;
            }
        }
        else if([callbackData isEqualToString:@"getContent"])
        {
            NSArray* contents = ([object objectForKey:@"contents"]);
            if(contents!=nil)
            {
                for(int i = 0 ; i< [contents count];i++)
                {
                    NSDictionary* content = [contents objectAtIndex:i];
                    if(content!=nil)
                    {
                        long cid = [[content objectForKey:@"id"] integerValue];
                        ContentStruct * cs = [[ContentStruct alloc]init];
                        NSString* strid =[NSString stringWithFormat:@"%ld",cid];
                        cs.id =strid;
                        cs.type = [content objectForKey:@"contenttype"];
                        cs.title = [content objectForKey:@"title"];
                        cs.subtitle = [content objectForKey:@"subtitle"];
                        cs.image = [content objectForKey:@"image"];
                        cs.thumbnail = [content objectForKey:@"thumbnail"];
                        cs.expirationDate = [content objectForKey:@"expirationdate"];
                        cs.content = [content objectForKey:@"content"];
                        cs.sharingtext = [content objectForKey:@"sharingtext"];
                        cs.sharinglink = [content objectForKey:@"sharinglink"];
                        [self.settings.contents setObject:cs forKey:strid];
                        if(i==contents.count-1)
                        {
                            self.settings.lastContent = strid;
                        }
                    }
                }
            }
        }
    }
}

-(void) setDeviceToken:(NSData*) token
{
    NSUserDefaults* defaults = NSUserDefaults.standardUserDefaults;
    NSString* deviceTokenString = [[[token description] stringByReplacingOccurrencesOfString:@"<" withString:@""] stringByReplacingOccurrencesOfString:@" " withString:@""];
    self.settings.token = deviceTokenString;
    [defaults setObject:deviceTokenString forKey:@"DeviceToken"];
    [defaults synchronize];
}

-(void) setNotificationToken:(NSString*) token {
    self.settings.notification_token = token;
    [self send_trackingWithtitle:@"Update Notification Token" AndStrClass:@"NOTIF" AndEventType:@"NOTIF" From:nil To:nil andDic:nil];
}

@end
