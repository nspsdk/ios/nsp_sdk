//
//  NspSdk_Objc.h
//  NspSdk_Objc
//
//  Created by maaks on 01/09/2017.
//  Copyright © 2017 maaks. All rights reserved.
//

#import <UIKit/UIKit.h>

//! Project version number for NspSdk.
FOUNDATION_EXPORT double NspSdkVersionNumber;

//! Project version string for NspSdk.
FOUNDATION_EXPORT const unsigned char NspSdkVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <NspSdk_Objc/PublicHeader.h>

#import <NspCore.h>
