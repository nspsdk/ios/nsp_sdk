//
//  DataToPush+CoreDataProperties.m
//  NSP_SDK
//
//  Created by Guillaume Gonzales on 30/12/2020.
//
//

#import "DataToPush+CoreDataProperties.h"

@implementation DataToPush (CoreDataProperties)

+ (NSFetchRequest<DataToPush *> *)fetchRequest {
	return [NSFetchRequest fetchRequestWithEntityName:@"DataToPush"];
}

@dynamic callback;
@dynamic data;
@dynamic url;

@end
