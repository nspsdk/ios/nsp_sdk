//
//  DataToPush+CoreDataProperties.h
//  NSP_SDK
//
//  Created by Guillaume Gonzales on 30/12/2020.
//
//

#import "DataToPush+CoreDataClass.h"


NS_ASSUME_NONNULL_BEGIN

@interface DataToPush (CoreDataProperties)

+ (NSFetchRequest<DataToPush *> *)fetchRequest;

@property (nullable, nonatomic, copy) NSString *callback;
@property (nullable, nonatomic, copy) NSString *data;
@property (nullable, nonatomic, copy) NSString *url;

@end

NS_ASSUME_NONNULL_END
