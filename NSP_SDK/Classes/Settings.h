//
//  Settings.h
//  NspSdk_Objc
//
//  Created by maaks on 14/04/2019.
//  Copyright © 2019 maaks. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ContentStruct.h"
NS_ASSUME_NONNULL_BEGIN

@interface Settings : NSObject

    @property NSString* clientId;
    @property NSString* appid;
    @property NSString* version;
    @property NSString* token;
    @property NSString* notification_token;
    @property NSString* pushUrl;
    @property NSInteger geozoneRefreshInterval;
    @property NSString* lastContent;
    @property NSMutableDictionary<NSString*,ContentStruct*>* contents;
    @property NSString* osLanguage;
    @property NSString* device;
    @property NSString* career;
    @property NSString* osVersion;
    @property NSString* appVersion;
    @property NSString* screeSize;
    @property NSString* ipAddress;
    @property NSString* sdkVersion;
    @property NSString* timeZone;
    @property Boolean wasSet;

-(id)init;
+(id) recursiveMutable:(id)object;
@end

NS_ASSUME_NONNULL_END
