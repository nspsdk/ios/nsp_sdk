//
//  URLSession+syncTask.m
//  NspSdk_Objc
//
//  Created by maaks on 15/04/2019.
//  Copyright © 2019 maaks. All rights reserved.
//

#import "URLSession+syncTask.h"

@implementation NSURLSession(syncTask)


+ (NSData *)synchronousDataTaskWithUrl:(NSURLRequest *)request
                 returningResponse:(__autoreleasing NSURLResponse **)responsePtr
                             error:(__autoreleasing NSError **)errorPtr
{
    dispatch_semaphore_t    sem;
    __block NSData *        result;
    
    result = nil;
    
    sem = dispatch_semaphore_create(0);
    
    [[[NSURLSession sharedSession] dataTaskWithRequest:request
                                     completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                         if (errorPtr != NULL) {
                                             *errorPtr = error;
                                         }
                                         if (responsePtr != NULL) {
                                             *responsePtr = response;
                                         }
                                         if (error == nil) {
                                             result = data;
                                         }
                                         dispatch_semaphore_signal(sem);
                                     }] resume];
    
    dispatch_semaphore_wait(sem, DISPATCH_TIME_FOREVER);
    
    return result;
}
/*

func synchronousDataTask(with url: URL) -> (Data?, URLResponse?, Error?) {
    var data: Data?
    var response: URLResponse?
    var error: Error?
    
    let semaphore = DispatchSemaphore(value: 0)
    
    let dataTask = self.dataTask(with: url) {
        data = $0
        response = $1
        error = $2
        
        semaphore.signal()
    }
    dataTask.resume()
    
    _ = semaphore.wait(timeout: .distantFuture)
    
    return (data, response, error)
}*/
@end
