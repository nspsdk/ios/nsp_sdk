//
//  Network.m
//  NspSdk_Objc
//
//  Created by maaks on 14/04/2019.
//  Copyright © 2019 maaks. All rights reserved.
//

#import "Network.h"
#import "NspCore.h"
#import "DataToPush+CoreDataClass.h"
#import "Settings.h"

@implementation Network

-(id)initWithCore:(NspCore*)core
{
    self.core = core;
    self.dtpList = [[NSMutableArray alloc]init];
    [self feedDtpList];
    [self networkLoop];
    
    return self;
}

-(void)feedDtpList
{
    if([NSProcessInfo processInfo].operatingSystemVersion.majorVersion>=10)
    {
        
        NSFetchRequest* fetchRequest = DataToPush.fetchRequest;
        NSManagedObjectContext* context = CoreDataStack.sharedInstance.persistentContainer.viewContext;
        NSError * err = nil;
        NSArray* searchResult = [context executeFetchRequest:fetchRequest error:&err];
        
        if(err!=nil)
        {
            if(searchResult!=nil && [searchResult count]>0)
            {
                for(int i = 0 ; i< [searchResult count];i++)
                {
                    DataToPush* dataToPush = [searchResult objectAtIndex:i];
                    [self.dtpList addObject:dataToPush];
                }
            }
        }
        
    }
}

-(void)synchronize:(id)lockObj WithBlock:(void(^)(void)) block
{
    @synchronized(lockObj)
    {
        block();
    }
}

-(void)send_dataWithParameterString: (NSString*)parameterString AndCallback:(NSString*)callback AndUrl:(NSString*)url
{
    if(_core.selectedGDPR == GDPR_NONE)
        return;
    
    // Prepare data
    NSData *data = [parameterString dataUsingEncoding:NSUTF8StringEncoding];
    id json = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
    NSMutableDictionary* cleanedData = [Settings recursiveMutable:json];
    
    if (_core.selectedGDPR == GDPR_ANON) {
        // Anonymize
        cleanedData[@"data"][@"content"][@"data"][0][@"login"] = cleanedData[@"data"][@"usersToken"];
        cleanedData[@"data"][@"content"][@"data"][0][@"_optin"] = @"0";
    } else if (_core.selectedGDPR == GDPR_FULL) {
        cleanedData[@"data"][@"content"][@"data"][0][@"_optin"] = @"1";
    }
    
    // Prepare back parameterString
    data = [NSJSONSerialization dataWithJSONObject:cleanedData options:0 error:nil];
    parameterString = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
    
    if([NSProcessInfo processInfo].operatingSystemVersion.majorVersion>=10)
    {
        NSManagedObjectContext* context = CoreDataStack.sharedInstance.persistentContainer.viewContext;
        NSEntityDescription* entity = [NSEntityDescription entityForName:@"DataToPush" inManagedObjectContext:context];
        NSManagedObject* dtp = [[NSManagedObject alloc]initWithEntity:entity insertIntoManagedObjectContext:context];
        [dtp setValue:parameterString forKey:@"data"];
        [dtp setValue:callback forKey:@"callback"];
        [dtp setValue:url forKey:@"url"];
        NSError* err = nil;
        [context save:&err];
        if(err==nil)
        {
            [self synchronize:self.dtpList WithBlock:^(void)
             {
                 [self.dtpList addObject:dtp];
             }];
        }
        else
        {
            NSLog(@"Could not save %@, %@",err,err.userInfo);
        }
        
        
    }
}
Boolean empty = false;
-(void)networkLoop
{
    empty = false;
    if([NSProcessInfo processInfo].operatingSystemVersion.majorVersion>=10)
    {
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            
            while(true)
            {
                if(!self.core.settings.wasSet)
                {
                    sleep(3);
                    continue;
                }
                [self synchronize:self.dtpList WithBlock:^{
                    if([self.dtpList count] >0)
                    {
                        DataToPush* dtp = [self.dtpList objectAtIndex:[self.dtpList count]-1];
                        NSString* urlString = dtp.url;
                        NSString* data = dtp.data;
                        NSString* callback = dtp.callback;
                        [self.dtpList removeLastObject];
                        
                        if(urlString==nil)return;
                        
                        NSURL * url = [[NSURL alloc] initWithString:urlString];
                        NSMutableURLRequest* request = [[NSMutableURLRequest alloc]initWithURL:url];
                        request.HTTPMethod = @"POST";
                        [request setValue:@"application/json" forHTTPHeaderField: @"Accept"];
                        [request setValue:@"application/json; charset=utf-8" forHTTPHeaderField: @"Content-Type"];
                        request.cachePolicy = NSURLRequestReloadIgnoringCacheData;
                        request.HTTPBody = [data dataUsingEncoding:NSUTF8StringEncoding];
                        
                        void (^completionHandler)(NSData * __nullable data, NSURLResponse * __nullable response, NSError * __nullable error);
                        completionHandler = ^(NSData * __nullable data, NSURLResponse * __nullable response, NSError * __nullable error)
                        {
                            if(error!=nil)
                            {
                                dispatch_async(dispatch_get_main_queue(), ^{
                                    [self synchronize:self.dtpList WithBlock:^
                                     {
                                         [self.dtpList addObject:dtp];
                                     }];
                                    NSLog(@"%@",error);
                                });
                            }
                            else
                            {
                                dispatch_async(dispatch_get_main_queue(), ^{
                                    NSManagedObjectContext* context = CoreDataStack.sharedInstance.persistentContainer.viewContext;
                                    [context deleteObject:dtp];
                                    NSError *err=nil;
                                    [context save:&err];
                                    if(err!=nil)
                                    {
                                        NSLog(@"error while removing dtp from dtplist");
                                    }
                                    else
                                    {
                                        [self.core callbackWithStringData:callback andData:data];
                                    }
                                });                                
                            }
                        };
                        NSURLSessionDataTask* task =  [[NSURLSession sharedSession] dataTaskWithRequest:request completionHandler:completionHandler];
                        [task resume];
                    }
                    else
                    {
                        empty = true;
                    }
                }];
                if(empty)
                    sleep(1);
                }
            }
        );
    }
}

@end
