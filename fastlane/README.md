fastlane documentation
================
# Installation

Make sure you have the latest version of the Xcode command line tools installed:

```
xcode-select --install
```

Install _fastlane_ using
```
[sudo] gem install fastlane -NV
```
or alternatively using `brew install fastlane`

# Available Actions
### increment_version
```
fastlane increment_version
```
Increment version number according to the [:type] (patch|minor|major)
### set_version
```
fastlane set_version
```
Set version number to [:version]
### set_nsp_spec
```
fastlane set_nsp_spec
```
Set the new [:version] in NSP_Spec and push

----

## iOS
### ios deploy
```
fastlane ios deploy
```


----

This README.md is auto-generated and will be re-generated every time [fastlane](https://fastlane.tools) is run.
More information about fastlane can be found on [fastlane.tools](https://fastlane.tools).
The documentation of fastlane can be found on [docs.fastlane.tools](https://docs.fastlane.tools).
