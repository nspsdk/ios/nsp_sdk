desc "Commit all the files using [:message]"
private_lane :commit do |options|
	git_commit(
		path: "*",
		message: "#{options[:message]}"
	)
end

desc "Create GIT tag using [:tag]"
private_lane :add_tag do |options|
	add_git_tag(tag: "#{options[:tag]}")
end

desc "Commit using [:message], create [:tag] and push to git remote"
private_lane :git_lane do |options|
	commit(message: "#{options[:message]}")
	add_tag(tag: "#{options[:tag]}")
	push_to_git_remote
end