desc "Set the new [:version] in NSP_Spec and push"
lane :set_nsp_spec do |options|
	version = "#{options[:version]}"
	sh "mkdir -p ../../NSP_Spec/NSP_SDK/" + version
    sh "cp ../NSP_SDK.podspec ../../NSP_Spec/NSP_SDK/" + version
    sh "cd ../../NSP_Spec && git add -- . && git commit -m 'Version " + version + "' && git push"
end