desc "Get version number"
private_lane :get_version do
	get_version_number(
    	xcodeproj: "Example/Pods/Pods.xcodeproj",
    	target: "NSP_SDK"
  	)
end

desc "Increment version number according to the [:type] (patch|minor|major)"
lane :increment_version do |options|
	increment_version_number(      
      bump_type: "#{options[:type]}",
      xcodeproj: "Example/Pods/Pods.xcodeproj"
    )
end

desc "Set version number to [:version]"
lane :set_version do |options|
	increment_version_number(
		version_number: "#{options[:version]}",
		xcodeproj: "Example/Pods/Pods.xcodeproj"
	)
	set_podspec_version(version: "#{options[:version]}")
end

desc "Set version in the Podspec file to [:version]"
private_lane :set_podspec_version do |options|
	version_bump_podspec(
		path: "NSP_SDK.podspec",
		version_number: "#{options[:version]}"
	)
end