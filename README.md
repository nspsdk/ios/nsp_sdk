# NSP_SDK

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.
**An APP ID is required in order to test the solution**

## Requirements

This SDK works with an **APP ID** provided by [SmartProfile](https://www.smartp.com/c.dspHome/smartprofile/)

## Installation

NSP_SDK is available through [CocoaPods](https://cocoapods.org). To install
it, simply add the following lines to your Podfile:

```ruby
source 'https://gitlab.com/nspsdk/ios/nsp_spec.git'

pod 'NSP_SDK'
```

## Author

devmobile@nsp-fr.com

## License

NSP_SDK is available under the MIT license. See the LICENSE file for more info.
